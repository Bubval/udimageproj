//
//  UIImageExtensions.swift
//  UserDefaultsProject
//
//  Created by Lubo on 11.06.19.
//  Copyright © 2019 Tumba. All rights reserved.
//

import UIKit

extension UIImage {
    func getCropRatio() -> CGFloat {
        let widthRatio = CGFloat(self.size.width / self.size.height)
        return widthRatio
    }
}
