//
//  ViewController.swift
//  FilePathApplication
//
//  Created by Lubo on 11.06.19.
//  Copyright © 2019 Tumba. All rights reserved.
//

import UIKit

class ImageTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    //MARK: - Properties
    let defaults = UserDefaults.standard
    let defaultsImgKey = "imgListArray"
    var imgDataArray = [Data]()
    
    
    //MARK: - App Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userDefaultsOfImgData = defaults.array(forKey: defaultsImgKey) as? [Data] {
            imgDataArray = userDefaultsOfImgData
        }
        
        self.tableView.register(imageTableViewCell.self, forCellReuseIdentifier: "imageTableViewCell")
    }
    
    //MARK: - Table Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imgDataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "imageTableViewCell", for: indexPath) as! imageTableViewCell
        let image = getUIImageFromData(data: imgDataArray[indexPath.row])
        cell.mainImageView.image = image
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                self?.imgDataArray.remove(at: indexPath.row)
                self?.syncImgDataWithUserDefaults()
                ProgressHUD.showSuccess("Deleted")
                DispatchQueue.main.async {
                    tableView.reloadData()
                }
            }
            ProgressHUD.show()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentImage = getUIImageFromData(data: imgDataArray[indexPath.row])
        let imageCrop = currentImage.getCropRatio()
        return tableView.frame.width / imageCrop
    }
    
    //MARK: - UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Cancel picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let data = image.pngData()! as NSData
            
            DispatchQueue.main.async {
                self?.imgDataArray.append(data as Data)
                self?.syncImgDataWithUserDefaults()
                self?.tableView.reloadData()
                ProgressHUD.showSuccess("Image Uploaded")
                self?.dismiss(animated: true, completion: nil)
            }
        }
        ProgressHUD.show()
    }
    
    //MARK: - Actions
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        //creates image picker controller
        let imagePickerController = UIImagePickerController()
        //opens gallery
        imagePickerController.sourceType = .photoLibrary
        //notifies controller when image picked
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: - Private Functions
    
    private func getUIImageFromData(data: Data) -> UIImage {
        return UIImage(data: data)!
    }
    
    private func syncImgDataWithUserDefaults() {
        self.defaults.set(self.imgDataArray, forKey: defaultsImgKey)
        self.imgDataArray = defaults.array(forKey: defaultsImgKey) as! [Data]
    }
}
